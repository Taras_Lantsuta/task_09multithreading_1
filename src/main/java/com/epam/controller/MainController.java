package com.epam.controller;

public interface MainController {

    void runPingPong();

    void showFibonacci();

    void showFibonacciSum();

    void runSleepTimer();

    void runMonitorTask();

    void runPipeTask();
}
