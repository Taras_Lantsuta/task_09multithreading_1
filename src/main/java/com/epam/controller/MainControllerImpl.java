package com.epam.controller;

import com.epam.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class MainControllerImpl implements MainController {

    private static Logger log = LogManager.getLogger(MainControllerImpl.class);
    private Scanner scanner = new Scanner(System.in);

    @Override
    public void runPingPong() {
        log.info("Enter number of repeats : ");
        new PingPong(scanner.nextInt()).doPingPong();
    }

    @Override
    public void showFibonacci() {
        log.info("Enter numbers of threads : ");
        int numbersOfThreads = scanner.nextInt();
        ExecutorService executorService = Executors.newFixedThreadPool(numbersOfThreads);
        for (int i = 1 ; i <= numbersOfThreads; i++){
            log.info("Enter count of fibonacci numbers : ");
            executorService.execute(
                    new Fibonacci(scanner.nextInt()).printFibonacciNumbers());
        }
        executorService.shutdown();
    }

    @Override
    public void showFibonacciSum() {
        log.info("Enter numbers of threads : ");
        int numbersOfThreads = scanner.nextInt();
        ExecutorService executorService = Executors.newFixedThreadPool(numbersOfThreads);
        for (int i = 1 ; i <= numbersOfThreads; i++){
            log.info("Enter count of fibonacci numbers : ");
            Future sum = executorService.submit(
                    new FibonacciSum(scanner.nextInt()).printFibonacciNumbers());
            try {
                log.info(sum.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        executorService.shutdown();
    }

    @Override
    public void runSleepTimer() {
        log.info("Enter task count : ");
        new SleepTimer().randomSleep(scanner.nextInt());
    }

    @Override
    public void runMonitorTask() {
        new MonitorTask().doTask();
    }

    @Override
    public void runPipeTask() {
        new PipeTask().doTask();
    }
}
