package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MonitorTask {

    private static Logger log = LogManager.getLogger(MonitorTask.class);
    private final Object firstObject = new Object();
    private final Object secondObject = new Object();
    private final Object thirdObject = new Object();

    public void doTask() {
        Thread first = new Thread(printFirst());
        Thread second = new Thread(printSecond());
        Thread third = new Thread(printThird());
        first.start();
        second.start();
        third.start();
    }

    private Runnable printFirst() {
        return () -> {
            synchronized (firstObject) {
                try {
                    Thread.sleep(2000);
                    log.info("First");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Runnable printSecond() {
        return () -> {
            synchronized (secondObject) {
                try {
                    Thread.sleep(2000);
                    log.info("Second");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Runnable printThird() {
        return () -> {
            synchronized (thirdObject) {
                try {
                    Thread.sleep(2000);
                    log.info("Third");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
    }
}
