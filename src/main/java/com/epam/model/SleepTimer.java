package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SleepTimer {

    private static Logger log = LogManager.getLogger(SleepTimer.class);
    private Random random;

    public SleepTimer() {
        random = new Random();
    }

    public void randomSleep(int count) {
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(count);
        for (int i = 1; i <= count; i++) {
            int time = random.nextInt(10) + 1;
            executorService.schedule(() -> log.info("Second : " + time), time, TimeUnit.SECONDS);
        }
        executorService.shutdown();
    }
}
