package com.epam.model;

import java.util.concurrent.Callable;

public class FibonacciSum {

    private int numbers;

    public FibonacciSum(int numbers) {
        this.numbers = numbers;
    }

    public Callable printFibonacciNumbers() {
        return () -> {
            synchronized (this) {
                int firstNumber = 1;
                int secondNumber = 1;
                int printNumber;
                int sum = 2;
                for (int i = 1; i <= numbers - 2; i++) {
                    printNumber = firstNumber + secondNumber;
                    sum += printNumber;
                    firstNumber = secondNumber;
                    secondNumber = printNumber;
                }
                return sum;
            }
        };
    }
}