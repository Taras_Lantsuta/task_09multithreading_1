package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Fibonacci {

    private static Logger log = LogManager.getLogger(Fibonacci.class);
    private int numbers;

    public Fibonacci(int numbers) {
        this.numbers = numbers;
    }

    public Runnable printFibonacciNumbers() {
        return () -> {
            synchronized (this) {
                String nameThread = Thread.currentThread().getName();
                int firstNumber = 1;
                log.info(nameThread + " " + firstNumber);
                int secondNumber = 1;
                log.info(nameThread + " " + secondNumber);
                int printNumber;
                for (int i = 1; i <= numbers - 2; i++) {
                    printNumber = firstNumber + secondNumber;
                    log.info(nameThread + " " + printNumber);
                    firstNumber = secondNumber;
                    secondNumber = printNumber;
                }
            }
        };
    }
}
