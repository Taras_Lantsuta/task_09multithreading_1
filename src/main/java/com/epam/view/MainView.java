package com.epam.view;

import com.epam.controller.MainController;
import com.epam.controller.MainControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MainView {

    private static Logger log = LogManager.getLogger(MainView.class);
    private MainController controller = new MainControllerImpl();
    private Map<String, String> menu;
    private Map<String, Runnable> methodRun;

    public MainView() {
        initMenu();
        methodRun = new LinkedHashMap<>();
        methodRun.put("pong", () -> controller.runPingPong());
        methodRun.put("fibonacci", () -> controller.showFibonacci());
        methodRun.put("sum" , () -> controller.showFibonacciSum());
        methodRun.put("sleep", () -> controller.runSleepTimer());
        methodRun.put("monitor", () -> controller.runMonitorTask());
        methodRun.put("pipe", () -> controller.runPipeTask());
        methodRun.put("exit", () -> log.info("Exit"));
    }

    public void runMenu() {
        Scanner scanner = new Scanner(System.in);
        StringBuilder flagExit = new StringBuilder("");
        do {
            menu.values().forEach(log::info);
            try {
                flagExit.setLength(0);
                flagExit.append(scanner.nextLine());
                methodRun.get(flagExit.toString()).run();
            } catch (NullPointerException e) {
                log.warn("Input correct option");
            }
        } while (!flagExit.toString().equals("exit"));
    }

    private void initMenu(){
        menu = new LinkedHashMap<>();
        menu.put("pong", "pong - Run PingPong");
        menu.put("fibonacci", "fibonacci - Show fibonacci numbers");
        menu.put("sum" , "sum - Show sum fibonacci numbers");
        menu.put("sleep", "sleep - Show thread sleep in second");
        menu.put("monitor", "monitor - Run monitor Task");
        menu.put("pipe", "pipe - Run Pipe task");
        menu.put("exit", "exit - Exit");
    }
}

